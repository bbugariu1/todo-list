import React from 'react';
import ReactDOM from 'react-dom';
import {store, persistor} from "./store/store";
import {Provider} from 'react-redux';
import App from './App/App';
import * as serviceWorker from './serviceWorker';
import theme from "./themes";
import {ThemeProvider} from "@material-ui/styles";
import { PersistGate } from 'redux-persist/integration/react';

ReactDOM.render(
    <React.StrictMode>
        <ThemeProvider theme={theme}>
            <Provider store={store}>
                <PersistGate loading={"Loading"} persistor={persistor}>
                    <App/>
                </PersistGate>
            </Provider>
        </ThemeProvider>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
