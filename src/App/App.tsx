import React from "react";
import AppDrawer from "../layout/Drawer/Drawer";
import Task from "../features/task/task";

const App = () => {
    return (
        <AppDrawer>
            <Task/>
        </AppDrawer>
    );
};

export default App;