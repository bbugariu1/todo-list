import {configureStore, combineReducers} from '@reduxjs/toolkit';
import counterReducer from '../components/Counter/Counter.slice';
import {taskReducer} from '../features/task/slice/task.slice';
import {useDispatch} from "react-redux";
import { persistStore, persistReducer } from 'redux-persist'
import logger from "redux-logger";import storage from 'redux-persist/lib/storage'

const persistConfig: any = {
    key: 'root',
    storage,
};

const rootReducer = combineReducers({
    counter: counterReducer,
    task: taskReducer
});

const persistedReducer = persistReducer(persistConfig, rootReducer);
export const store =  configureStore({
    reducer: persistedReducer,
    middleware: [logger]
});
export const persistor = persistStore(store);


export type RootState = ReturnType<typeof rootReducer | any>;
export type AppDispatch = typeof store.dispatch
export const useAppDispatch = () => useDispatch<AppDispatch>() // Export a hook that can be reused to resolve types
