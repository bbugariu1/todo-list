import React from 'react';
import {ITask} from "../slice/task.slice";
import {
    Grid,
    List as MuiList,
} from "@material-ui/core";
import TaskListItem from "./taskListItem";

interface IProps {
    list: Array<ITask>
}

const TaskList = ({list}: IProps) => {

    const listItems = () => {
        return list.map((task: ITask) => <TaskListItem key={task.id} task={task}/>);
    };

    return (
        <div>
            <Grid container spacing={2}>
                <MuiList dense style={{width: "inherit"}}>
                    {listItems()}
                </MuiList>
            </Grid>
        </div>

    );
};

export default TaskList;