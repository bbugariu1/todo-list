import React from 'react';
import {
    Checkbox,
    IconButton,
    ListItemAvatar,
    ListItemSecondaryAction,
    ListItemText,
    ListItem
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from '@material-ui/icons/Edit';
import {useDispatch} from "react-redux";
import Modal from "../../../components/Modal/Modal";
import {ITask, remove} from "../slice/task.slice";

interface IProps {
    task: ITask
}


const TaskListItem = ({task}: IProps) => {
    const [checked, setChecked] = React.useState<boolean>(false);
    const [open, setOpen] = React.useState<boolean>(false);

    const dispatch = useDispatch();

    const handleClose = () => {
        setOpen(false);
    }

    const handleRemoveClick = () => {
        setOpen(true)
    }

    const handleRemove = () => {
        dispatch(remove(task.id));
        setOpen(false);
    };

    const handleChange = () => {

    };

    return (
        <>
            <ListItem key={task.id}>
                <ListItemAvatar>
                    <Checkbox
                        checked={checked}
                        onChange={handleChange}
                        inputProps={{'aria-label': 'primary checkbox'}}
                    />
                </ListItemAvatar>
                <ListItemText primary={task.title} secondary={task.body} style={{maxWidth:"85%", textAlign: "justify"}}/>
                <ListItemSecondaryAction>
                    <IconButton edge="end" aria-label="delete" onClick={handleRemoveClick}>
                        <DeleteIcon/>
                    </IconButton>
                    <IconButton edge="end" aria-label="edit">
                        <EditIcon/>
                    </IconButton>
                </ListItemSecondaryAction>
            </ListItem>

            <Modal
                title={"Are tou sure that you want to delete this beautiful Task?"}
                open={open}
                onAgree={() => dispatch(remove(task.id))}
                onDisagree={() => setOpen(false)}
                onClose={() => setOpen(false)}
            />
        </>
    );
};

export default TaskListItem;