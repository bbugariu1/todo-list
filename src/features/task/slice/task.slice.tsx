import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {RootState} from "../../../store/store";

export interface IState {
    list: Array<ITask> | []
}

export interface ITask {
    id: number
    title: string
    body: string
    completed?: boolean
}

const initialState: IState = {
    list: []
};

// Create the slice
export const taskSlice = createSlice({
    initialState: initialState,
    name: "task",
    reducers: {
        create: (state, action: PayloadAction<ITask>) => {
            const tasks: Array<ITask> = state.list;
            const task = action.payload;
            task.id = state.list.length + 1;
            tasks.push(task);

            state.list = tasks;
        },

        remove: (state, action: PayloadAction<number>) => {
            state.list = state.list.filter((task) => task.id !== action.payload);
        }
    }

});

// Export actions
export const { create, remove } = taskSlice.actions;

// Export reducer
export const taskReducer = taskSlice.reducer;

// Export list
export const selectList = (state: RootState) => state.task.list;