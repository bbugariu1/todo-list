import React from "react";
import {Box, createStyles, Theme, Typography} from "@material-ui/core";
import {useSelector} from "react-redux";
import {RootState} from "../../store/store";
import {makeStyles} from "@material-ui/core/styles";
import TaskList from "./taskList/taskList";
import CreateTask from "./create/create";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
            maxWidth: 680,
            margin: "0 auto"
        }
    }),
);

const Task = () => {
    const classes = useStyles();
    const list = useSelector((state: RootState) => state.task.list);

    return (
        <div className={classes.root}>
            <Box mb={2}>
                <Typography variant={"h4"}>
                    Task's
                </Typography>
                <CreateTask/>
            </Box>

            <TaskList list={list}/>
        </div>
    );
};

export default Task;