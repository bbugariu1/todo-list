import React, {ChangeEvent} from "react";
import Modal from "../../../components/Modal/Modal";
import useModal from "../../../components/Modal/useModal";
import {Button, Grid} from "@material-ui/core";
import {create, ITask} from "../slice/task.slice";
import {useDispatch} from "react-redux";
import TextField from "@material-ui/core/TextField";

interface IProps {
}

const CreateTask = ({}: IProps) => {
    const {isOpen, onDisagree, onAgree, onClose, toggle} = useModal(() => onCreate());
    const [values, setValues] = React.useState<ITask>({id: 0, title: "", body: ""});
    const dispatch = useDispatch();

    const onCreate = () => {
        if (isValid()) {
            dispatch(create(values));
        }
    };

    /**
     * TODO: Make a more reasonably validator
     */
    const isValid = () => {
      return !!(values.body.length > 0 && values.body.length > 0);
    };

    const handleChange = (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        event.preventDefault();

        const name = event.currentTarget.name;
        const value = event.currentTarget.value;
        setValues({...values, [name] : value});
    };

    return (
        <>
            <Button variant={"contained"} size={"small"} color={"primary"} onClick={toggle}>
                Add Task
            </Button>

            <Modal title={"Create a new task"} open={isOpen} onClose={onClose} onAgree={onAgree}
                   onDisagree={onDisagree}>
                <form noValidate={true} autoComplete={"off"}>
                    <Grid container>
                        <Grid item xs={12}>
                            <TextField
                                onChange={handleChange}
                                id={"title"}
                                name={"title"}
                                label={"title"}
                                fullWidth={true}/>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                onChange={handleChange}
                                id={"body"}
                                name={"body"}
                                label={"content"}
                                multiline={true}
                                rows={2}
                                fullWidth={true}/>
                        </Grid>
                    </Grid>
                </form>
            </Modal>
        </>
    );
};

export default CreateTask;