import React from "react";

const useModal = (callback?: Function) => {
    const [isOpen, setIsOpen] = React.useState<boolean>(false);

    const toggle = () => setIsOpen(!isOpen);

    const onClose = () => {
        console.log("onClose");
        setIsOpen(false);
    };

    const onAgree = () => {
        if (callback) {
            return callback()
        }
        console.log("onAgree");
    };

    const onDisagree = () => {
        console.log("onDisagree");
        setIsOpen(false)
    };

    return {
        isOpen,
        onClose,
        onAgree,
        onDisagree,
        toggle
    };
};

export default useModal;