import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
    increment,
    decrement,
    incrementByAmount,
    incrementAsync,
    selectCount
} from "./Counter.slice";
import {Button} from "@material-ui/core";

const Counter = () => {
    const count = useSelector(selectCount);
    const dispatch = useDispatch();
    const [incrementAmount, setIncrementAmount] = React.useState(2);

    return (
        <div>Counter
            <Button variant={"contained"} onClick={() => dispatch(increment())}>
                Increment
            </Button>
            <span>{count}</span>
            <Button variant={"contained"} onClick={() => dispatch(decrement())}>
                Decrement
            </Button>

            <Button variant={"contained"} onClick={() => dispatch(incrementByAmount(20))}>
                increment by 20
            </Button>
        </div>
    );
};

export default Counter;